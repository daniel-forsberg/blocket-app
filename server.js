const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 8080 });

const members = [];

const broadcast = (data, ws) => {
  wss.clients.forEach(client => {
    if (client.readyState === WebSocket.OPEN && client !== ws) {
      client.send(JSON.stringify(data));
    }
  });
};

wss.on('connection', ws => {
  let index;
  ws.on('message', message => {
    const data = JSON.parse(message);
    switch (data.type) {
      case 'ADD_MEMBER': {
        index = members.length;
        members.push({ alias: data.alias, id: index + 1 });
        ws.send(
          JSON.stringify({
            type: 'MEMBERS_LIST',
            members
          })
        );
        broadcast(
          {
            type: 'MEMBERS_LIST',
            members
          },
          ws
        );
        break;
      }
      case 'ADD_MESSAGE':
        broadcast(
          {
            type: 'ADD_MESSAGE',
            message: data.message,
            alias: data.alias
          },
          ws
        );
        break;
      default:
        break;
    }
  });

  ws.on('close', () => {
    members.splice(index, 1);
    broadcast(
      {
        type: 'MEMBERS_LIST',
        members
      },
      ws
    );
  });
});
