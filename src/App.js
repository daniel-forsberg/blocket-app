import React, { Component } from 'react';

import AddMessage from 'components/AddMessage';
import MessageList from 'components/MessageList';
import MembersList from 'components/MembersList';

import styled from 'styled-components';

const Root = styled.div`
  display: grid;
  width: 100%;
  height: 100vh;
  grid-template-columns: 1fr 3fr;
  grid-template-areas: 'members main';
`;

const Main = styled.main`
  grid-area: main;
`;

class App extends Component {
  render() {
    return (
      <Root>
        <MembersList />
        <Main>
          <MessageList />
          <AddMessage />
        </Main>
      </Root>
    );
  }
}

export default App;
