import generateAlias from 'utils/generateAlias';
import * as types from './types';

let messageId = 0;
let memberId = 0;

export const addSocket = socket => {
  return { type: types.ADD_SOCKET, socket };
};

export const addMessage = ({ message, alias }) => {
  return { type: types.ADD_MESSAGE, id: messageId++, message, alias };
};

export const addMember = () => {
  return { type: types.ADD_MEMBER, id: memberId++, alias: generateAlias() };
};

export const messageReceived = ({ message, alias }) => ({
  type: types.MESSAGE_RECEIVED,
  id: messageId++,
  message,
  alias
});

export const populateMembersList = members => ({
  type: types.MEMBERS_LIST,
  members
});
