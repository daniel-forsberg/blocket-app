import * as actions from './actions';

const actionsCreator = Object.assign({}, actions);

export default actionsCreator;
