export const ADD_MESSAGE = 'ADD_MESSAGE';
export const ADD_MEMBER = 'ADD_MEMBER';
export const MESSAGE_RECEIVED = 'MESSAGE_RECEIVED';
export const MEMBERS_LIST = 'MEMBERS_LIST';
export const ADD_SOCKET = 'ADD_SOCKET';
