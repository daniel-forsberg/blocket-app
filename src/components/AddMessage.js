import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actions from 'actions';

import { AddMessageContainer, TextInput } from './styles';

class AddMessage extends Component {
  state = {
    inputValue: ''
  };

  submitMessage = e => {
    e.preventDefault();
    const inputValue = this.state.inputValue;
    if (inputValue.length > 0) {
      this.props.actions.addMessage({ message: inputValue, alias: this.props.members.current.alias });
      this.setState({ inputValue: '' });
    }
  };

  updateValue = e => {
    this.setState({ inputValue: e.target.value });
  };

  render() {
    return (
      <AddMessageContainer>
        <form onSubmit={this.submitMessage}>
          <TextInput autoFocus value={this.state.inputValue} onChange={this.updateValue} />
          <input type="submit" />
        </form>
      </AddMessageContainer>
    );
  }
}

function mapStateToProps({ members }) {
  return {
    members
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddMessage);
