import React, { Component } from 'react';
import { connect } from 'react-redux';

import { MembersListContainer, List, Member } from './styles';

class MembersList extends Component {
  render() {
    return (
      <MembersListContainer>
        <List>
          {this.props.members.list.map(m => (
            <Member key={m.id}>{m.alias}</Member>
          ))}
        </List>
      </MembersListContainer>
    );
  }
}

function mapStateToProps({ members }) {
  return {
    members
  };
}

export default connect(mapStateToProps)(MembersList);
