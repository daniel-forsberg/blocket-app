import React from 'react';
import PropTypes from 'prop-types';
import { Message } from './styles';

const MessageItem = ({ alias, message, recieved }) => {
  return (
    <Message recieved={recieved}>
      <p>
        {recieved ? alias : 'Me'}: {message}
      </p>
    </Message>
  );
};

MessageItem.propTypes = {
  alias: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  recieved: PropTypes.bool.isRequired
};

export default MessageItem;
