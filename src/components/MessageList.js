import React, { Component } from 'react';
import { connect } from 'react-redux';

import MessageItem from 'components/MessageItem';
import { MessageListContainer, List } from './styles';

class MessageList extends Component {
  render() {
    return (
      <MessageListContainer>
        <List>
          {this.props.messages.map(m => (
            <MessageItem recieved={m.recieved} key={m.id} message={m.message} alias={m.alias} />
          ))}
        </List>
      </MessageListContainer>
    );
  }
}

function mapStateToProps({ messages }) {
  return {
    messages
  };
}

export default connect(mapStateToProps)(MessageList);
