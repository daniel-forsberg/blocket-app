import styled from 'styled-components';
import { space } from 'utils/styleUtils';

export const List = styled.ul`
  padding: 0;
  margin: 0;
`;

export const Message = styled.li`
  list-style-type: none;
  border: 2px solid ${props => (props.recieved ? 'blue' : 'green')};
  border-radius: 6px;
  padding: ${space(1)};
  margin-bottom: ${space(1)};
  p {
    margin: 0;
  }
`;

export const MessageListContainer = styled.section`
  padding: ${space(1)};
`;

export const AddMessageContainer = styled.section`
  border-top: 2px solid green;
  position: fixed;
  bottom: 0;
  width: 100%;
  padding: ${space(1)};
`;

export const TextInput = styled.input`
  width: 100%;
  border: 0;
  font-weight: bold;
  height: 2em;
  &:focus {
    outline: 0;
  }
`;

export const MembersListContainer = styled.aside`
  grid-area: members;
  border-right: 2px solid black;
  height: 100%;
  padding: ${space(1)};
`;

export const Member = styled.li`
  list-style-type: none;
  padding: ${space(1)};
  margin-bottom: ${space(1)};
  p {
    margin: 0;
  }
`;
