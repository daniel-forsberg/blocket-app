import * as types from 'actions/types';

export function socketMiddleware() {
  return store => next => action => {
    next(action);

    if (action.type === types.ADD_MESSAGE) {
      const socket = store.getState().socket;
      socket.send(JSON.stringify(action));
    }

    if (action.type === types.ADD_MEMBER) {
      const socket = store.getState().socket;
      socket.send(JSON.stringify(action));
    }
  };
}
