import { combineReducers } from 'redux';
import messageReducer from './messageReducer';
import memberReducer from './memberReducer';
import socketReducer from './socketReducer';

const rootReducer = combineReducers({
  messages: messageReducer,
  members: memberReducer,
  socket: socketReducer
});

export default rootReducer;
