import createReducer from 'utils/createReducer';
import * as types from 'actions/types';

const initialState = {
  current: {},
  list: []
};

export default createReducer(initialState, {
  [types.ADD_MEMBER](state, action) {
    return {
      ...state,
      current: action,
      list: state.list.concat([
        {
          id: action.id,
          alias: action.alias
        }
      ])
    };
  },
  [types.MEMBERS_LIST](state, action) {
    return {
      ...state,
      list: action.members
    };
  }
});
