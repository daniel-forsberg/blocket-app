import createReducer from 'utils/createReducer';
import * as types from 'actions/types';

const initialState = [];

export default createReducer(initialState, {
  [types.MESSAGE_RECEIVED](state, action) {
    return state.concat([
      {
        id: action.id,
        message: action.message,
        alias: action.alias,
        recieved: true
      }
    ]);
  },
  [types.ADD_MESSAGE](state, action) {
    return state.concat([
      {
        id: action.id,
        message: action.message,
        alias: action.alias,
        recieved: false
      }
    ]);
  }
});
