import createReducer from 'utils/createReducer';
import * as types from 'actions/types';

const initialState = null;

export default createReducer(initialState, {
  [types.ADD_SOCKET](_, action) {
    return action.socket;
  }
});
