import { messageReceived, populateMembersList, addMember, addSocket } from 'actions/actions';
import * as types from 'actions/types';

export function initializeSocket() {
  return dispatch => {
    const socket = new WebSocket('ws://localhost:8080');
    socket.onopen = function() {
      dispatch(addSocket(socket));
      dispatch(addMember());
    };

    socket.onmessage = function(event) {
      const data = JSON.parse(event.data);
      switch (data.type) {
        case types.ADD_MESSAGE:
          const { message, alias } = data;
          dispatch(messageReceived({ message, alias }));
          break;
        case types.MEMBERS_LIST:
          dispatch(populateMembersList(data.members));
          break;
        default:
          break;
      }
    };
  };
}
