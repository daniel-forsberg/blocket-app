import { createLogger } from 'redux-logger';
import rootReducer from './reducers';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import { initializeSocket } from 'socket';
import { socketMiddleware } from 'middleware';

const enhancer = compose(applyMiddleware(thunk, createLogger(), socketMiddleware()));
const initialState = {};
const store = createStore(rootReducer, initialState, enhancer);

store.dispatch(initializeSocket());

export default store;
