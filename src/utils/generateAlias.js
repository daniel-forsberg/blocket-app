import Chance from 'chance';

export default function() {
  const chance = new Chance();
  return chance.name();
}
