export const space = size => {
  return 6 * size + (size !== 0 ? 'px' : '');
};
